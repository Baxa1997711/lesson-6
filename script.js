class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
class Baby extends Person {
    constructor(name, age, favToy) {
     super(name, age);
     this.toy = favToy;
    }
    play() {
        console.log(`${this.name} is ${this.age} years old, and playing with ${this.toy}`);
    }
}

const firstBaby = new Baby('Bob', 2, 'Rocket Toy');
// firstBaby.play();


// Task 2

class Person2 {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    describe() {
        console.log(`${this.name} is ${this.age} years old`);
    }
}

const jack = new Person2('Jack', 25);
const jill = new Person2('Jill', 24);
// jack.describe();
// jill.describe();